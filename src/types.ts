import {UserSortBy} from "./constants/constants";
import {IUserInfo} from "./models/IUser";

export interface ISortValue {
  value: number,
  name: keyof typeof UserSortBy | ''
}

export interface IPaginatedInfo {
  data: IUserInfo[][]
  pages: number
  currPage: number
  nextPage: boolean
  prevPage: boolean
}
