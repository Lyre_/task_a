import React from 'react';
import ReactDOM from 'react-dom/client';
import {CssBaseline} from "@mui/material";
import App from './App';
import {Provider} from "react-redux";
import store from "./store/store";
import {StyledEngineProvider} from "@mui/material/styles";
import {BrowserRouter} from "react-router-dom";

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <Provider store={store}>
    <BrowserRouter>
      <StyledEngineProvider injectFirst>
        <CssBaseline/>
        <App/>
      </StyledEngineProvider>
    </BrowserRouter>
  </Provider>
);
