import React, {useMemo} from 'react';
import ArrowBackIosOutlinedIcon from "@mui/icons-material/ArrowBackIosOutlined";
import {IconButton, SxProps} from "@mui/material";

interface ArrowBtnProps {
  rotate?: boolean
  className?: string
  onClick?: () => void
  active?: boolean
}

const ArrowBtn = ({rotate, className, onClick, active}: ArrowBtnProps) => {
  const sxStyles = useMemo(() => {
    const styles: SxProps = {
      transform: rotate ? 'rotate(90deg)' : 'rotate(-90deg)',
      transition: "all 0.3s ease",
      transitionProperty: "transform background",
    }
    if (active) {
      styles.background = active ? '#5e6172' : 'palette.primary'
    }
    return (styles);
  }, [active, rotate])

  return (
    <IconButton className={className}
                sx={sxStyles}
                onClick={onClick}
    >
      <ArrowBackIosOutlinedIcon/>
    </IconButton>
  );
};

export default ArrowBtn;