import React from 'react';
import {Grid} from "@mui/material";
import THead from "./THead/THead";
import {SortHow, UserSortBy} from "../../../constants/constants";
import {ISortValue} from "../../../types";
import styled from "styled-components";

interface THeadWrapperProps {
  sortValue: ISortValue
}

const TableHeader = ({sortValue}: THeadWrapperProps) => {
  return (
    <THeadGrid container p={1}
               justifyContent={"space-between"}
    >
      <Grid item xs={1}>
        <THead
          title={'ID'} isSorted={sortValue.name === UserSortBy.id}
          sortedHow={sortValue.name === UserSortBy.id && sortValue.value > 0 ? SortHow.desc : SortHow.asc}
          sortBy={UserSortBy.id}
        />
      </Grid>
      <Grid item xs={5}>
        <THead
          title={'Заголовок'} isSorted={sortValue.name === UserSortBy.title}
          sortedHow={sortValue.name === UserSortBy.title && sortValue.value > 0 ? SortHow.desc : SortHow.asc}
          sortBy={UserSortBy.title}
        />
      </Grid>
      <Grid item xs={6}>
        <THead
          title={'Описание'} isSorted={sortValue.name === UserSortBy.body}
          sortedHow={sortValue.name === UserSortBy.body && sortValue.value > 0 ? SortHow.desc : SortHow.asc}
          sortBy={UserSortBy.body}
        />
      </Grid>
    </THeadGrid>
  );
};

const THeadGrid = styled(Grid)`
  background: #474955;
  color: white;
`

export default TableHeader;