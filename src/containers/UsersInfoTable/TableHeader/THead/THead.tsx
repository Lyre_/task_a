import React, {useCallback} from 'react';
import {Grid, Typography} from "@mui/material";
import styled from "styled-components";
import ArrowBtn from "../../../../components/UI/Buttons/ArrowBtn/ArrowBtn";
import {SortHow, UserSortBy} from "../../../../constants/constants";
import {useAppDispatch} from "../../../../hooks/reduxHooks";
import {sortInfo} from "../../../../store/actions/usersActions";

interface ITHead {
  title: string
  sortBy: keyof typeof UserSortBy
  isSorted: boolean
  sortedHow: typeof SortHow["desc" | "asc"]
}

const THead = ({title, isSorted, sortedHow, sortBy}: ITHead) => {
  const dispatch = useAppDispatch();
  const onArrowBtnClick = useCallback(() => {
    const sort = {value: 1, name: sortBy}
    if (SortHow.desc === sortedHow) {
      sort.value = -1
    }
    dispatch(sortInfo(sort))
  }, [sortedHow, dispatch, sortBy])

  return (
    <Grid container item
          alignItems={"center"}
          justifyContent={"space-between"}
          flexWrap={"nowrap"}
    >
      <Grid item flexGrow={1} textAlign={"center"}>
        <Typography>{title}</Typography>
      </Grid>
      <Grid item>
        <ArrowBtnSC onClick={onArrowBtnClick} active={isSorted} rotate={SortHow.desc === sortedHow}/>
      </Grid>
    </Grid>
  );
};

const ArrowBtnSC = styled(ArrowBtn)`
  margin: 4px;
  color: inherit;

  &:hover {
    background-color: #5e6172;
  }
`

export default THead;