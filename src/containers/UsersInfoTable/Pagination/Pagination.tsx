import React from 'react';
import {Button, Grid, Link} from "@mui/material";
import {Link as RouterLink} from "react-router-dom";
import {USERS_INFO_URL} from "../../../constants/nav_urls";
import {IPaginatedInfo} from "../../../types";
import styled from "styled-components";

interface PaginationProps {
  paginatedInfo: IPaginatedInfo
}

const Pagination = ({paginatedInfo}: PaginationProps) => {
  return (
    <PaginationGrid container px={4} py={2}
                    justifyContent={"space-between"}
                    alignItems={"center"}
    >
      <Grid item xs={12} sm={"auto"}>
        <Button disabled={!paginatedInfo.prevPage}
                component={RouterLink}
                sx={{color: "#474955"}}
                to={USERS_INFO_URL + (paginatedInfo.currPage - 1)}
        >
          Назад
        </Button>
      </Grid>
      <PagesGrid item xs={12} sm={"auto"}>
        <div>
          {paginatedInfo.data.map((item, i) => (
            <Link key={i + 'pageBtn'}
                  component={RouterLink}
                  to={USERS_INFO_URL + (i + 1)}
                  sx={{color: paginatedInfo.currPage === i + 1 ? '#7EBC3C' : '#474955'}}
            >
              {i + 1}
            </Link>
          ))}
        </div>
      </PagesGrid>
      <Grid item xs={12} sm={"auto"}>
        <Button disabled={!paginatedInfo.nextPage}
                component={RouterLink}
                sx={{color: "#474955"}}
                to={USERS_INFO_URL + (paginatedInfo.currPage + 1)}
        >
          Далее
        </Button>
      </Grid>
    </PaginationGrid>
  );
};

const PaginationGrid = styled(Grid)`
  font-size: 24px;
  font-weight: 500;
`

const PagesGrid = styled(Grid)`
  flex-grow: 1;
  font-size: 18px;
  font-style: italic;
  font-weight: bold;

  & a {
    text-decoration: none;
    padding: 0 3px;
    cursor: pointer;

    &:hover {
      color: #63942f;
    }
  }

  & > div {
    display: flex;
    justify-content: center;
  }
`

export default Pagination;