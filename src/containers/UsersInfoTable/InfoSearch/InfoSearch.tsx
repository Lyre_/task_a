import React, {FormEvent, useState} from 'react';
import styled from "styled-components";
import {IconButton} from "@mui/material";
import SearchIcon from '@mui/icons-material/Search';
import {searchUsersInfo, setInfoCurrPage} from "../../../store/actions/usersActions";
import {useAppDispatch} from "../../../hooks/reduxHooks";
import {useNavigate} from "react-router-dom";


const InfoSearch = () => {
  const [search, setSearch] = useState('Поиск')
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const onSearchSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    navigate('../1', {replace: true})
    dispatch(setInfoCurrPage(1));
    dispatch(searchUsersInfo(search));
    setSearch("")
  }
  const onSearchChange = (e: FormEvent<HTMLInputElement>) => {
    setSearch(e.currentTarget.value)
  }

  return (
    <SearchWrapper onSubmit={onSearchSubmit}>
      <Search value={search} onChange={onSearchChange}/>
      <IconButton sx={{color: "white", position: "absolute", right: "10px", top: "6px"}}
                  type={"submit"}
      >
        <SearchIcon/>
      </IconButton>
    </SearchWrapper>
  );
};

const Search = styled.input`
  background: #5A5C66;
  width: 100%;
  border: none;
  height: 52px;
  padding: 0 52px 0 26px;
  color: #B2B7BF;
`

const SearchWrapper = styled.form`
  position: relative;
  max-width: 630px;
`
export default InfoSearch;