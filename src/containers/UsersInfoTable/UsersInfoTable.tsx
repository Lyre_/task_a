import React, {useEffect} from 'react';
import {useAppDispatch, useAppSelector} from "../../hooks/reduxHooks";
import {fetchUsersInfo} from "../../store/actions/usersActions";
import {Box, Container, Grid, Stack} from "@mui/material";
import styled from "styled-components";
import UsersSlice from "../../store/slices/usersSlice";
import InfoSearch from "./InfoSearch/InfoSearch";
import {useParams} from "react-router-dom";
import TableHeader from "./TableHeader/TableHeader";
import Pagination from "./Pagination/Pagination";

const {setInfoCurrPage} = UsersSlice.actions;

const UsersInfoTable = () => {
  const {info, infoLoading, sortValue, paginatedInfo} = useAppSelector(state => state.users)
  const params = useParams();
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(fetchUsersInfo())
  }, [dispatch]);

  useEffect(() => {
    dispatch(setInfoCurrPage(parseInt(params.page || '1')))
  }, [params.page, info, dispatch]);

  return (
    <Container sx={{my: 2}}>
      <InfoSearch/>
      <Box sx={{my: 2, minWidth: "460px"}}>
        <TableHeader sortValue={sortValue}/>
        {infoLoading && (<div>Loading...</div>)}
        {paginatedInfo && (
          <>
            <TRowsStack>
              {paginatedInfo.data[paginatedInfo.currPage - 1].map(item => (
                <RowGrid container key={item.keyId}>
                  <Grid item xs={1}>
                    <Content>{item.userId}</Content>
                  </Grid>
                  <Grid item xs={5}>
                    <Content>{item.title}</Content>
                  </Grid>
                  <Grid item xs={6}>
                    <Content>{item.body}</Content>
                  </Grid>
                </RowGrid>
              ))}
            </TRowsStack>
            <Pagination paginatedInfo={paginatedInfo}/>
          </>
        )}
      </Box>
    </Container>
  );
};

const TRowsStack = styled(Stack)`
  min-height: 700px;
  border: 1px solid #E3E6EC;
`

const RowGrid = styled(Grid)`
  & > div {
    display: flex;
    padding: 10px;
    border: 1px solid #E3E6EC;
  }
`
const Content = styled.div`
  display: -webkit-box;
  margin: auto;
  overflow: hidden;
  text-overflow: ellipsis;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
  text-align: center;
`
export default UsersInfoTable;