export enum UserSortBy {
  title = 'title',
  body = 'body',
  id = 'id',
}
export enum SortHow {
  desc = 'desc',
  asc = 'asc',
}