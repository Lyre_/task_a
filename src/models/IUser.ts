export interface IUserInfo {
  userId: number,
  id: number,
  title: string,
  body: string,
  keyId: string,
}