import React from 'react';
import UsersInfoTable from "./containers/UsersInfoTable/UsersInfoTable";
import {Route, Routes} from "react-router-dom";
import {USERS_INFO_URL} from "./constants/nav_urls";

const App = () => (
  <Routes>
    <Route path={USERS_INFO_URL} element={<UsersInfoTable/>} />
    <Route path={USERS_INFO_URL + ":page"} element={<UsersInfoTable/>} />
    <Route path="*" element={<div>Ooops</div>} />
  </Routes>

);

export default App;
