import {combineReducers, configureStore} from "@reduxjs/toolkit";
import usersSlice from "./slices/usersSlice";

const rootReducer = combineReducers({
users: usersSlice.reducer
})
const store = configureStore({
    devTools: true,
    reducer: rootReducer
  })

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
export type AppStore = typeof store

export default store