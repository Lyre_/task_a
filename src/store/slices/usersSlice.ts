import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {IUserInfo} from "../../models/IUser";
import {IError} from "../../models/IError";
import {IPaginatedInfo, ISortValue} from "../../types";

const name = 'users';

export const initialState: UsersState = {
  info: [],
  paginatedInfo: null,
  infoLoading: false,
  infoErr: null,
  searchData: null,
  sortValue: {value: 0, name: ''}
};

const usersSlice = createSlice({
  name,
  initialState,
  reducers: {
    setPaginationInfo(state, action: PayloadAction<IUserInfo[]>) {
      const data: IUserInfo[][] = [];
      let count = 0
      do {
        data.push(action.payload.slice(count, 10 + count))
        count += 10
      } while (action.payload.length > count)

      const pages = data.length;
      const currPage = state.paginatedInfo?.currPage ? state.paginatedInfo.currPage : 1;
      const nextPage = currPage < pages;
      const prevPage = currPage > 1;

      state.paginatedInfo = {data, pages, currPage, nextPage, prevPage}
    },
    setInfoCurrPage(state, action: PayloadAction<number>) {
      if (state.paginatedInfo) {
        if ((state.paginatedInfo.pages >= action.payload) && (action.payload > 1)) {
          let allowNext = true
          if (state.paginatedInfo.pages === action.payload) {
            allowNext = false
          }
          state.paginatedInfo.currPage = action.payload
          state.paginatedInfo.nextPage = allowNext
          state.paginatedInfo.prevPage = true
        } else {
          state.paginatedInfo.currPage = 1
          state.paginatedInfo.prevPage = false
          state.paginatedInfo.nextPage = true
        }
      }
    },
    setSortValue(state, action: PayloadAction<ISortValue>) {
      state.sortValue = action.payload;
    },
    setSearchData(state, action: PayloadAction<IUserInfo[]>) {
      state.searchData = action.payload;
    },
    fetchUsersInfoRequest(state) {
      state.infoErr = null;
      state.infoLoading = true;
    },
    fetchUserInfoSuccess(state, action: PayloadAction<IUserInfo[]>) {
      state.info = action.payload;
      state.infoLoading = false;
    },
    fetchUserInfoFailure(state, action: PayloadAction<IError>) {
      state.infoErr = action.payload
      state.infoLoading = false;
    },
  },
});
interface UsersState {
  info: IUserInfo[]
  paginatedInfo: IPaginatedInfo | null
  infoLoading: boolean
  infoErr: null | IError
  sortValue: ISortValue
  searchData: IUserInfo[] | null
}

export default usersSlice;
