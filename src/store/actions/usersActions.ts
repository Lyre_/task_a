import {AppDispatch, AppStore} from "../store";
import axios from "axios";
import {IUserInfo} from "../../models/IUser";
import usersSlice from "../slices/usersSlice";
import {usersUrl} from "../../constants/api_urls";
import {UserSortBy} from "../../constants/constants";
import {ISortValue} from "../../types";

export const fetchUsersInfo = () => async (dispatch: AppDispatch) => {
  try {
    dispatch(fetchUsersInfoRequest())
    const {data} = await axios.get<IUserInfo[]>(usersUrl.info)
    data.forEach(item => {
      item.keyId = Math.random().toString()
    })
    dispatch(setPaginationInfo(data))
    dispatch(fetchUserInfoSuccess(data))
  } catch (err) {
    if (axios.isAxiosError(err)) {
      dispatch(fetchUserInfoFailure(err))
    }
  }
}
export const sortInfo = (sort: ISortValue) => (dispatch: AppDispatch, getState: AppStore["getState"]) => {
  dispatch(setSortValue(sort))
  let resData = [...getState().users.info]
  switch (sort.name) {
    case UserSortBy.id: {
      resData.sort((a, b) => {
        if (a.userId < b.userId) return -1;
        if (a.userId > b.userId) return 1;
        return 0
      })
      break;
    }
    case UserSortBy.title: {
      resData.sort((a, b) => {
        if (a.title < b.title) return -1;
        if (a.title > b.title) return 1;
        return 0
      })
      break;
    }
    case UserSortBy.body: {
      resData.sort((a, b) => {
        if (a.body < b.body) return -1;
        if (a.body > b.body) return 1;
        return 0
      })
      break;
    }
  }
  if (sort.value < 0) {
    resData.reverse()
  }
  dispatch(setPaginationInfo(resData))
}
export const searchUsersInfo = (search: string) => (dispatch: AppDispatch, getState: AppStore["getState"]) => {
  if (!getState().users.searchData) dispatch(setSearchData(getState().users.info))
  const {searchData} = getState().users
  if (searchData) {
    const filteredData = searchData.filter(item => new RegExp(search, 'i').test(item.body) || new RegExp(search, 'i').test(item.title))
    dispatch(fetchUserInfoSuccess(filteredData))
    dispatch(sortInfo(getState().users.sortValue))
  }
}

export const {
  fetchUsersInfoRequest,
  fetchUserInfoSuccess,
  fetchUserInfoFailure,
  setPaginationInfo,
  setSortValue,
  setSearchData,
  setInfoCurrPage
} = usersSlice.actions;